<?php

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Security\StaticTrustedCallbackHelper;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 *
 */
function entitylogic_token_info_alter(&$data) {
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if (isset($data['tokens'][$entity_type_id])) {
      $data['tokens'][$entity_type_id]['entitylogic'] = [
        'name' => 'Entitylogic',
        'description' => 'Entitylogic',
        'dynamic' => TRUE,
      ];
    }
  }
}

/**
 *
 */
function entitylogic_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type === 'entity' && !empty($data['entity_type']) && !empty($data['entity'])) {
    $path_tokens = \Drupal::token()->findWithPrefix($tokens, 'entitylogic');

    if (!empty($path_tokens)) {
      $wEntity = entitylogic($data['entity']);
      foreach ($path_tokens as $path => $original) {
        if (method_exists($wEntity, $path)) {
          $replacements[$original] = StaticTrustedCallbackHelper::callback(
            [$wEntity, $path],
            [],
            'Message',
            TrustedCallbackInterface::TRIGGER_SILENCED_DEPRECATION
          );
        }
      }
    }

    return $replacements;
  }
}
