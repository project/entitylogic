<?php

namespace Drupal\entitylogic;

/**
 * Defines an interface for B14State plugin label plugins.
 */
interface EntityLogicInterface {

  /**
   * Get the entity instance.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity();

  /**
   * Create a new entity instance.
   *
   * @param array $values
   *   The values passed along to the instance.
   */
  public function createEntity(array $values = []);

}
