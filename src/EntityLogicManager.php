<?php

namespace Drupal\entitylogic;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager.
 */
class EntityLogicManager extends DefaultPluginManager {

  public const DEFAULT_FALLBACK_ID = '_fallback';

  /**
   * Static cache for logic instances.
   *
   * This way the same logic instance is used everywhere, allowing for states in
   * the objects.
   *
   * @var EntityLogicInterface[]
   *
   * @todo Handle reloads of the entities, so they also update the static cache.
   */
  protected $instances = [];

  /**
   * Construct.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EntityLogic', $namespaces, $module_handler, 'Drupal\entitylogic\EntityLogicInterface', 'Drupal\entitylogic\Annotation\EntityLogic');
    $this->alterInfo('entitylogic');
    $this->setCacheBackend($cache_backend, 'entitylogic');
  }

  public function resetCache(EntityInterface $entity) {
    unset($this->instances[$entity->uuid()]);

    return $this;
  }

  /**
   * Wrap an entity inside an EntityLogic class.
   */
  public function wrapEntity(EntityInterface $entity, $selector = NULL, $configuration = [], $fallback_id = self::DEFAULT_FALLBACK_ID) {
    $info = [
      'entity_type' => $entity->getEntityTypeId(),
      'bundle' => $entity->getEntityType()->getKey('bundle') ? $entity->bundle() : NULL,
      'selector' => $selector
    ];
    $plugin_id = $this->selectPluginId($info['entity_type'], $info['bundle'], $info['selector'], $fallback_id);

    if ($plugin_id) {
      $ckey = $entity->uuid();
      if (!isset($this->instances[$ckey][$plugin_id])) {
        $this->instances[$ckey][$plugin_id] = $this->createInstance($plugin_id, ['info' => $info] + $configuration)
          ->setEntity($entity);
      }

      return $this->instances[$ckey][$plugin_id];
    }
  }

  /**
   * Create an entity and wrap it.
   */
  public function wrapNew($input, $values = [], $configuration = [], $fallback_id = self::DEFAULT_FALLBACK_ID) {
    $info = $this->decodePluginId($input);
    $plugin_id = $this->selectPluginId($info['entity_type'], $info['bundle'], $info['selector'], $fallback_id);

    if ($plugin_id) {
      $instance = $this->createInstance($plugin_id, ['info' => $info] + $configuration)->createEntity($values);
      return $this->updateInstanceStaticCache($instance, $plugin_id);
    }
  }

  /**
   * Get a new instance, but without any entity yet.
   */
  public function wrapEmpty($input, $configuration = [], $fallback_id = self::DEFAULT_FALLBACK_ID) {
    $info = $this->decodePluginId($input);
    $plugin_id = $this->selectPluginId($info['entity_type'], $info['bundle'], $info['selector'], $fallback_id);

    if ($plugin_id) {
      return $this->createInstance($plugin_id, ['info' => $info] + $configuration);
    }
  }

  /**
   * Get an existing or create a new entity.
   */
  public function provideWrap($input, array $filter, $configuration = [], $fallback_id = self::DEFAULT_FALLBACK_ID) {
    $info = $this->decodePluginId($input);

    if (!empty($info['bundle'])) {
      $entity_type = \Drupal::entityTypeManager()->getDefinition($info['entity_type']);
      if ($bundle_key = $entity_type->getKey('bundle')) {
        $filter[$bundle_key] = $info['bundle'];
      }
    }

    $storage = \Drupal::entityTypeManager()->getStorage($info['entity_type']);
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->range(0, 1);
    foreach ($filter as $field => $value) {
      $query->condition($field, $value);
    }

    $ids = $query->execute();
    if (empty($ids)) {
      return $this->wrapNew($input, $filter, $configuration, $fallback_id);
    }
    else {
      return $this->wrapEntity($storage->load(reset($ids)), $info['selector']);
    }
  }

  /**
   * Insert the instance into the static cache.
   */
  public function updateInstanceStaticCache($instance, $plugin_id = NULL) {
    if ($plugin_id === NULL) {
      $definition = $this->getDefinitionByClass(get_class($instance));
      $plugin_id = $definition['plugin_id'];
    }

    $ckey = $instance->getEntity()->uuid();
    if (!isset($this->instances[$ckey][$plugin_id])) {
      $this->instances[$instance->getEntity()->uuid()][$plugin_id] = $instance;
    }

    return $instance;
  }

  /**
   * Get the EntityLogic class definition.
   */
  public function getLogicClass($input, $fallback_id = self::DEFAULT_FALLBACK_ID) {
    $info = $this->decodePluginId($input);
    $plugin_id = $this->selectPluginId($info['entity_type'], $info['bundle'], $info['selector'], $fallback_id);

    if ($plugin_id) {
      return DefaultFactory::getPluginClass($plugin_id, $this->getDefinition($plugin_id), $this->pluginInterface);
    }
  }

  /**
   * Decode an input string.
   *
   * Pattern is [entity_type.bundle:selector].
   *
   * @param string $input
   *   An input string.
   *
   * @return array
   *   String decoded into an array.
   */
  public function decodePluginId($input) {
    $data = ['entity_type' => NULL, 'bundle' => NULL, 'selector' => NULL];

    $input = explode(':', $input);
    if (!empty($input[1])) {
      $data['selector'] = $input[1];
    }

    $input = explode('.', $input[0]);
    if (!empty($input[1])) {
      $data['bundle'] = $input[1];
    }

    $data['entity_type'] = $input[0];

    return $data;
  }

  /**
   * Create a plugin id.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle
   *   Bundle.
   * @param string $selector
   *   Selector.
   */
  public function encodePluginId($entity_type_id, $bundle = NULL, $selector = NULL) {
    $id = $entity_type_id;

    if (!empty($bundle)) {
      $id .= '.' . $bundle;
    }

    if (!empty($selector)) {
      $id .= ':' . $selector;
    }

    return $id;
  }

  /**
   * Get the plugin id matching the criteria.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle
   *   Bundle.
   * @param string $selector
   *   Selector.
   * @param string $fallback_id
   *   Fallback ID.
   */
  public function selectPluginId($entity_type_id, $bundle = NULL, $selector = NULL, $fallback_id = self::DEFAULT_FALLBACK_ID) {
    $definitions = $this->getDefinitions();

    $plugin_ids = [
      $this->encodePluginId($entity_type_id, $bundle, $selector),
      $this->encodePluginId($entity_type_id, NULL, $selector),
    ];

    if (!empty($fallback_id)) {
      $plugin_ids[] = $this->encodePluginId($fallback_id, NULL, $selector);
    }

    foreach ($plugin_ids as $plugin_id) {
      if (isset($definitions[$plugin_id])) {
        return $plugin_id;
      }
    }
  }

  /**
   * The the definition with the given class name.
   *
   * Include full namespace.
   */
  public function getDefinitionByClass($class_name) {
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if ($definition['class'] === $class_name) {
        return $definition + ['plugin_id' => $plugin_id];
      }
    }
  }

}
