<?php

namespace Drupal\entitylogic\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a B14EntityHandler annotation object.
 *
 * @Annotation
 */
class EntityLogic extends Plugin {

  /**
   * Entity type ID.
   *
   * @var string
   */
  public $entity_type;

  /**
   * Entity bundle.
   *
   * @var string
   */
  public $bundle;

  /**
   * Selector.
   *
   * @var string
   */
  public $selector;

  /**
   * {@inheritdoc}
   *
   * The ID entity, bundle and selector dependent.
   */
  public function getId() {
    if (isset($this->definition['id'])) {
      return $this->definition['id'];
    }

    return entitylogic()->encodePluginId(
      $this->definition['entity_type'],
      isset($this->definition['bundle']) ? $this->definition['bundle'] : NULL,
      isset($this->definition['selector']) ? $this->definition['selector'] : NULL
    );
  }

}
