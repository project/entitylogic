<?php

namespace Drupal\entitylogic\Generators;

use DrupalCodeGenerator\Command\ModuleGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Question\Question;

/**
 *
 */
class EntityLogicGenerator extends ModuleGenerator {

  protected string $name = 'entitylogic';
  protected string $description = 'Generates an entitylogic.';
  protected string $templatePath = __DIR__;

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars): void {
    $this->collectDefault($vars);

    $this->entityAndBundleQuestions($vars);
    $vars['selector'] = $this->ask('Selector', '');

    $vars['class_name'] = Utils::camelize($vars['entity_type_id'])
      . Utils::camelize($vars['bundle'] ?? '')
      . Utils::camelize($vars['selector'] ?? '');

    $this->addFile('src/Plugin/EntityLogic/{class_name}.php', 'entitylogic.php.twig');
  }

  protected function entityAndBundleQuestions(array &$vars) {
    $entity_type_id = new Question('Entity Type ID');
    $entity_type_id->setAutocompleterValues(array_keys(\Drupal::entityTypeManager()->getDefinitions()));
    $vars['entity_type_id'] = $this->io->askQuestion($entity_type_id);

    $bundle = new Question('Bundle');
    $bundle->setAutocompleterValues(array_keys(\Drupal::service('entity_type.bundle.info')->getBundleInfo($vars['entity_type_id'])));

    $vars['bundle'] = $this->io->askQuestion($bundle);
  }

}
