<?php

namespace Drupal\entitylogic;

use Drupal\Core\Entity\EntityInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 *
 */
class EntityLogicTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('entitylogic', 'entitylogic'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'entitylogic_twig_extension';
  }

}
