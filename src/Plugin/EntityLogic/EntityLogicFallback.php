<?php

namespace Drupal\entitylogic\Plugin\EntityLogic;

use Drupal\entitylogic\EntityLogicBase;

/**
 * Simple entityLogic fallback class.
 *
 * If no other EntityLogic plugin exists this is the default fallback.
 *
 * @EntityLogic(
 *   id = "_fallback",
 * )
 */
class EntityLogicFallback extends EntityLogicBase {

}
