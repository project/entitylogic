<?php

namespace Drupal\entitylogic;

use Drupal\Component\Plugin\PluginBase;
use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extend this class for a basic EntityLogic implementation.
 */
class EntityLogicBase extends PluginBase implements EntityLogicInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Instance of EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Holds the original entity values, from when the object was instantiated.
   *
   * This is used in saveChanges, so the entity is only saved to the database if
   * there's any changes.
   * Having this holder array is not as safe as using the loadUnchanged() load
   * function, and only saving if there's any changes.
   *
   * @var array
   * @see saveChanges()
   */
  protected $originalValues = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Change the entity to another translation.
   *
   * @param string $langcode
   *   The language code to switch to.
   *   If not supplied it's get the current language.
   * @param bool $create
   *   Set TRUE to create missing translation.
   */
  public function translate(string $langcode = NULL, bool $create = FALSE) {
    if ($this->entity instanceof TranslatableInterface) {
      if ($langcode === NULL) {
        $this->entity = \Drupal::service('entity.repository')->getTranslationFromContext($this->entity);
      }
      else {
        if (!$this->entity->hasTranslation($langcode) && $create === TRUE) {
          $values = $this->entity->toArray();
          unset($values['content_translation_source']);
          $translation = $this->entity->addTranslation($langcode, $this->entity->toArray());

          // Stolen from ContentTranslationController::prepareTranslation():
          // Make sure we do not inherit the affected status from the source values.
          if ($this->entity->getEntityType()->isRevisionable()) {
            $translation->setRevisionTranslationAffected(NULL);
          }

          // Also from ContentTranslationController::prepareTranslation():
          /** @var ContentTranslationManagerInterface */
          $content_translation__manager = \Drupal::service('content_translation.manager');
          $metadata = $content_translation__manager->getTranslationMetadata($translation);

          $metadata->setSource($this->entity->language()->getId());

          $translation->save();
        }
        $this->entity = \Drupal::service('entity.repository')->getTranslationFromContext($this->entity, $langcode);
      }
    }

    return $this;
  }

  public function getCacheableMetadata() {
    // Drupal does not automatically apply entity tags to fields, so do this.
    return CacheableMetadata::createFromObject($this->entity);
  }

  /**
   * Set the entity.
   */
  public function setEntity(EntityInterface $entity, bool $reset = FALSE) {
    if (isset($this->entity) && $reset === FALSE) {
      throw new \LogicException('Entity already set');
    }

    $this->entity = $entity;
    $this->originalValues = $this->toArray();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity(array $values = []) {
    $info = $this->configuration['info'];

    if (!empty($info['bundle'])) {
      $entity_type = \Drupal::entityTypeManager()->getDefinition($info['entity_type']);
      $values[$entity_type->getKey('bundle')] = $info['bundle'];
    }

    $this->setEntity(\Drupal::entityTypeManager()->getStorage($info['entity_type'])->create($values));

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Normalize data from $this->entity->toArray().
   *
   * - Removes empty values, custom properties (prefixed "_") and computed
   *   objects.
   * - Types all values as string.
   * - Sorts the field properties by key.
   */
  protected function normalizeArray(array $values = NULL, EntityInterface $entity = NULL): array {
    if ($entity === NULL) {
      $entity = $this->getEntity();
    }
    if ($entity instanceof FieldableEntityInterface) {
      foreach ($values as $field_name => $items) {
        $field_list = $entity->get($field_name);

        if ($field_list->isEmpty()) {
          $values[$field_name] = [];
          continue;
        }

        foreach ($items as $delta => $properties) {
          foreach ($properties as $property_name => $property) {
            if ($property === null) {
              unset($values[$field_name][$delta][$property_name]);
            }
            elseif ($property_name[0] === '_') {
              unset($values[$field_name][$delta][$property_name]);
            }
            elseif (is_object($property)) {
              unset($values[$field_name][$delta][$property_name]);
            }
            elseif (!is_array($property)) {
              $values[$field_name][$delta][$property_name] = (string) $property;
            }
          }

          ksort($values[$field_name][$delta]);
        }
      }
    }

    return $values;
  }

  /**
   * Get a normalized array of all the entity translations.
   *
   * Used to check for changes to the entity.
   */
  public function toArray(): array {
    $values = [];

    if ($this->entity instanceof TranslatableInterface) {
      foreach ($this->entity->getTranslationLanguages() as $language) {
        $translation = $this->entity->getTranslation($language->getId());
        $values[$language->getId()] = $this->normalizeArray($translation->toArray(), $translation);
      }
    }
    else {
      $values = $this->normalizeArray($this->entity->toArray());
    }

    return $values;
  }

  /**
   * Check if a anything has changed.
   */
  public function hasChanges(): bool {
    return json_encode($this->originalValues) !== json_encode($this->toArray());
  }

  /**
   * Save the entity.
   */
  public function saveChanges(): bool {
    if ($this->entity->isNew() || $this->hasChanges()) {
      $this->entity->save();
      $this->originalValues = $this->toArray();
      return TRUE;
    }

    return FALSE;
  }

}
