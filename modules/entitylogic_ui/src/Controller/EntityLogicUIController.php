<?php

namespace Drupal\entitylogic_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\entitylogic\EntityLogicManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EntityLogicUIController extends ControllerBase {

  /** @var EntityLogicManager */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityLogicManager $manager) {
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entitylogic')
    );
  }

  /**
   * Overview of logic classes.
   */
  public function listPage() {
    $build['existing'] = [
      '#type' => 'table',
      '#header' => ['Provider','Plugin ID', 'Entity Type', 'Bundle', 'Selector', 'Class'],
      '#sticky' => TRUE,
    ];

    foreach ($this->manager->getDefinitions() as $plugin_id => $definition) {
      $class_parts = explode('\\', $definition['class']);
      $class_parts[4] = '<strong>' . $class_parts[4] . '</strong>';

      $build['existing'][] = [
        ['#plain_text' => $definition['provider']],
        Link::createFromRoute($plugin_id, 'entitylogic_ui.class', ['plugin_id' => $plugin_id])->toRenderable(),
        ['#plain_text' => $definition['entity_type'] ?? '' ],
        ['#plain_text' => $definition['bundle'] ?? '' ],
        ['#plain_text' => $definition['selector'] ?? '' ],
        ['#markup' => implode('\\', $class_parts)],
      ];
    }

    return $build;
  }

  /**
   * Information about specific class.
   */
  public function logicPage($plugin_id) {
    // @todo Extend this to show public, protected, private, static, return
    //       values, documentation, and more.
    // @todo Do custom styling instead of the simple tables.
    $definition = $this->manager->getDefinition($plugin_id);
    $reflection = new \ReflectionClass($definition['class']);

    $build = [
      'local_functions' => [
        '#type' => 'table',
        '#header' => ['Method', 'Parameters'],
        '#sticky' => TRUE,
      ],
      'inherited_functions' => [
        '#type' => 'table',
        '#header' => ['Method', 'Parameters', 'Owner'],
        '#sticky' => TRUE,
      ]
    ];

    foreach ($reflection->getMethods() as $method) {
      $row = [
        ['#plain_text' => $method->getName()],
      ];

      $parameters = [];
      foreach ($method->getParameters() as $parameter) {
        $parameters[] = $parameter->getName();
      }
      $row[] = ['#plain_text' => implode(', ', $parameters)];

      if ($method->class === $definition['class']) {
        $build['local_functions'][] = $row;
      }
      else {
        $row[] = ['#plain_text' => $method->class];
        $build['inherited_functions'][] = $row;
      }
    }

    return $build;
  }

  /**
   * Page title
   */
  public function logicPageTitle($plugin_id) {
    return $plugin_id;
  }

}
